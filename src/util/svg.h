#pragma once

#include <QPixmap>
#include <QSize>
#include <QString>

namespace net::tibault::util {

QPixmap svgToPixmap(const QString& filePath,
                    const QSize& size);
QPixmap svgToPixmap(const QByteArray& contents,
                    const QSize& size);

}

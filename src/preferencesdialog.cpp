#include "preferencesdialog.h"

#include <QAbstractButton>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QSettings>

namespace net::tibault::qronos {

PreferencesDialog::PreferencesDialog(std::shared_ptr<jira::JiraApi> api,
                                     QWidget *parent)
    : QDialog(parent),
      _api(std::move(api))
{
    auto authGrp = new QGroupBox(tr("Atlassian Authentication"), this);

    _domain = new QLineEdit(this);
    _domain->setMinimumWidth(200);
    _domain->setPlaceholderText("<company>.atlassian.net");

    _user = new QLineEdit(this);
    _user->setMinimumWidth(200);

    _token = new QLineEdit(this);
    _token->setMinimumWidth(200);
    _token->setEchoMode(QLineEdit::PasswordEchoOnEdit);

    auto help = new QLabel(this);
    help->setTextFormat(Qt::RichText);
    help->setOpenExternalLinks(true);
    auto helpText = QStringLiteral("<a href=\"%1\">%2</a>")
            .arg("https://id.atlassian.com/manage-profile/security/api-tokens",
                 tr("Where to find the token?"));
    help->setText(helpText);
    help->setToolTip(tr("This will open the Atlassian site, "
                        "where you can generate a token."));

    auto form = new QFormLayout(authGrp);
    form->addRow(tr("Domain:"), _domain);
    form->addRow(tr("E-Mail:"), _user);
    form->addRow(tr("Token:"), _token);
    form->addWidget(help);

    auto auth = _api->authInfo();
    _domain->setText(auth.domain);
    _user->setText(auth.email);
    _token->setText(auth.token);

    auto statusGrp = new QGroupBox(tr("Status"), this);
    auto statusLayout = new QGridLayout(statusGrp);
    _status = new QLabel(tr("Connecting..."), this);
    statusLayout->addWidget(_status);
    updateStatus(_api->whoAmI());

    auto trackingGrp = new QGroupBox(tr("Time tracking"), this);
    auto trackingLayout = new QGridLayout(trackingGrp);
    _pauseOnLock = new QCheckBox(tr("Only track time when screen "
                                    "is unlocked"), this);
    _pauseOnLock->setToolTip(tr("Don't track time spent staring at "
                                "your screensaver"));
    trackingLayout->addWidget(_pauseOnLock);

    QSettings s;
    _pauseOnLock->setChecked(s.value("qronos/pauseOnLock", true).toBool());

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel
                                     | QDialogButtonBox::Apply,
                                     this);
    connect(btns, &QDialogButtonBox::clicked,
            this, [=](QAbstractButton* btn){
        switch (btns->buttonRole(btn)) {
        case QDialogButtonBox::AcceptRole:
        case QDialogButtonBox::ApplyRole:
            save();
            break;
        default:
            break;
        }
    });
    connect(btns, &QDialogButtonBox::accepted,
            this, &PreferencesDialog::accept);
    connect(btns, &QDialogButtonBox::rejected,
            this, &PreferencesDialog::reject);

    auto layout = new QGridLayout(this);
    layout->addWidget(authGrp);
    layout->addWidget(statusGrp);
    layout->addWidget(trackingGrp);
    layout->addWidget(btns);
}

void PreferencesDialog::updateStatus(const jira::promise<jira::User> &info)
{
    QPointer self(this);
    info.then([self](const jira::User& user) {
        if (self) {
            if (user.id.isEmpty()) {
                self->_status->setText(tr("Not logged in."));
            } else {
                self->_status->setText(tr("Logged in as %1.").arg(user.name));
            }
        }
    })
    .fail([self](const jira::UnauthenticatedException&){
        if (self) {
            self->_status->setText(tr("Invalid credentials!"));
        }
    })
    .fail([self](){
        if (self) {
            self->_status->setText(tr("Unknown error: check domain?"));
        }
    });
}

void PreferencesDialog::save()
{
    if (_domain->isModified()
            || _user->isModified()
            || _token->isModified()) {
        _status->setText(tr("Connecting..."));
        updateStatus(_api->login({
            _domain->text().trimmed(),
            _user->text().trimmed(),
            _token->text().trimmed()
        }));
        auto auth = _api->authInfo();
        _domain->setText(auth.domain);
        _user->setText(auth.email);
        _token->setText(auth.token);
    }

    QSettings s;
    s.setValue("qronos/pauseOnLock",
               _pauseOnLock->isChecked());
}

}

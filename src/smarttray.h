#pragma once

#include "statusbanner.h"
#include "timetracker.h"

#include <QMenu>
#include <QSystemTrayIcon>

namespace net::tibault::qronos {

class SmartTray : public QSystemTrayIcon
{
    Q_OBJECT

public:
    explicit SmartTray(TimeTracker* tracker,
                       std::shared_ptr<jira::JiraApi> api);
    ~SmartTray() override;

    void updateMenu();
    void showBanner(bool show);
    void openPreferences();

    void startTracking(int64_t id);

private:
    StatusBanner* _banner;
    TimeTracker* _tracker;
    std::shared_ptr<jira::JiraApi> _api;

    QMenu _trayMenu;

    QAction* _currentIssue;
    QAction* _stopTracking;
    QAction* _showIssue;
    QAction* _findIssue;
    QMenu* _suggestedIssues;
    QAction* _showBanner;
    QAction* _preferences;
};

}

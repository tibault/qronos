#include "findissuedialog.h"
#include "jira/issuedelegate.h"
#include "util/svg.h"

#include <QComboBox>
#include <QCheckBox>
#include <QDesktopServices>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QGridLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QMenu>
#include <QPushButton>

static const int minimum_width{250};

namespace net::tibault::qronos {

IssueResultModel::IssueResultModel(const std::shared_ptr<jira::JiraApi> &api,
                                   QObject *parent)
    : QAbstractListModel(parent),
      _api(api)
{
    // empty
}

void IssueResultModel::suggestIssues(const QString &query,
                                     const QString &project,
                                     bool assignedToMe)
{
    QPointer self(this); // lifetime guard
    _api->findIssues(query, project, assignedToMe)
    .then([self](const QVector<jira::Issue>& issues){
        if (self) {
            self->beginResetModel();
            self->_issues = issues;
            self->endResetModel();
        }
    });
}

int IssueResultModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : _issues.size();
}

QVariant IssueResultModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()
            || index.parent().isValid()) {
        return {};
    }

    if (index.row() >= _issues.size())
        return {};

    switch (role) {
    case Qt::DisplayRole:
        return _issues[index.row()].summary;
    case Qt::EditRole:
        return _issues[index.row()].key;
    case Qt::ToolTipRole:
        return QStringLiteral("%1 - %2").arg(
            _issues[index.row()].key,
            _issues[index.row()].summary
        );
    case Qt::DecorationRole:
        return fetchIcon(_issues[index.row()].img);

    case Roles::IdRole:
        return QVariant::fromValue(_issues[index.row()].id);
    case Roles::UrlRole:
        return _api->webUrl(_issues[index.row()]);

    default:
        return {};
    }
}

QIcon IssueResultModel::fetchIcon(const QString &url) const
{
    if (auto it = _iconCache.constFind(url); it != _iconCache.constEnd()) {
        return it.value();
    } else {
        _iconCache.insert(url, {});

        QUrl req{url};
        if (req.isValid()) {
            QPointer self{this};
            _api->getBlob(req)
            .then([url, self](const QByteArray& data){
                if (!self)
                    return;

                QIcon ico{util::svgToPixmap(data, {64, 64})};
                if (ico.isNull())
                    return;

                self->_iconCache[url] = ico;
                auto rows = self->rowCount();
                if (rows > 0) {
                    auto mself = const_cast<IssueResultModel*>(self.data());
                    mself->dataChanged(self->index(0),
                                       self->index(rows - 1),
                                       { Qt::DecorationRole });
                }
            })
            .fail([url, self](){
                if (self) self->_iconCache.remove(url);
            });
        }

        return {};
    }
}

FindIssueDialog::FindIssueDialog(const std::shared_ptr<jira::JiraApi> &api,
                                 QWidget *parent)
    : QDialog(parent)
    , _api(api)
    , _selectedIssue(0)
{
    setWindowTitle(tr("Find issue..."));

    auto search = new QLineEdit(this);
    search->setMinimumWidth(minimum_width);
    search->setPlaceholderText(tr("Search Jira"));
    search->setFocus();

    auto projects = new QComboBox(this);
    projects->setMinimumWidth(minimum_width);
    projects->addItem("-- Any Project --", {});

    auto mine = new QCheckBox(tr("Show only tickets assigned to me"), this);

    results = new QListView(this);
    auto model = new IssueResultModel(_api, this);
    results->setModel(model);
    results->setItemDelegate(new jira::IssueDelegate);
    results->setContextMenuPolicy(Qt::CustomContextMenu);

    QTimer* searchDebouncer = new QTimer(this);
    searchDebouncer->setSingleShot(true);
    searchDebouncer->setInterval(500);
    connect(search, &QLineEdit::textEdited,
            searchDebouncer, [=](){
        searchDebouncer->start(500);
    });
    connect(projects, QOverload<int>::of(&QComboBox::activated),
            this, [=](int) {
        searchDebouncer->start(0);
    });
    connect(mine, &QCheckBox::toggled,
            this, [=](){
        searchDebouncer->start(0);
    });
    connect(searchDebouncer, &QTimer::timeout,
            this, [=](){
        model->suggestIssues(search->text(),
                             projects->currentData().toString(),
                             mine->isChecked());
    });

    auto btns = new QDialogButtonBox(this);
    connect(btns, &QDialogButtonBox::accepted,
            this, &FindIssueDialog::accept);
    connect(btns, &QDialogButtonBox::rejected,
            this, &FindIssueDialog::reject);
    auto okButton = btns->addButton(tr("Start tracking"),
                                    QDialogButtonBox::AcceptRole);
    okButton->setEnabled(false);
    btns->addButton(QDialogButtonBox::Cancel);

    btns->addButton(tr("Create new issue..."),
                    QDialogButtonBox::ActionRole)
        ->setEnabled(false); // TODO

    auto updateSelection = [=](){
        auto selection = results->selectionModel()->selectedIndexes();
        if (selection.isEmpty()) {
            _selectedIssue = 0;
            okButton->setEnabled(false);
        } else {
            _selectedIssue = selection.first()
                                      .data(IssueResultModel::Roles::IdRole)
                                      .toLongLong();
            okButton->setEnabled(true);
        }
    };

    connect(model, &IssueResultModel::modelReset,
            this, updateSelection);
    connect(results->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, updateSelection);
    connect(results, &QListView::customContextMenuRequested,
            this, [=](const QPoint& pos) {
        if (auto index = results->indexAt(pos); index.isValid()) {
            auto url = index.data(IssueResultModel::Roles::UrlRole)
                            .toUrl();
            if (url.isValid()) {
                QMenu menu;
                menu.addAction(tr("Open in browser..."),
                               this, [&](){
                    if (QDesktopServices::openUrl(url)) {
                        menu.close();
                    }
                });
                menu.exec(results->mapToGlobal(pos));
            }
        }
    });

    auto layout = new QGridLayout(this);

    layout->addWidget(search, 0, 0);
    layout->addWidget(projects, 1, 0);
    layout->addWidget(mine, 2, 0, Qt::AlignHCenter);
    layout->addWidget(results, 3, 0);
    layout->addWidget(btns, 4, 0);

    setLayout(layout);

    setMinimumWidth(420);

    searchDebouncer->start(0);

    QPointer self{projects};
    _api->listProjects()
    .then([self](const QVector<jira::Project>& list){
        if (!self || list.isEmpty())
            return;

        self->insertSeparator(self->count());
        for (const auto& p : list) {
            self->addItem(QStringLiteral("[%1] %2").arg(p.key, p.name),
                          p.id);
        }
    });
}

int64_t FindIssueDialog::selectedIssue() const
{
    return _selectedIssue;
}

void FindIssueDialog::keyPressEvent(QKeyEvent* evt)
{
    if (focusWidget() != results || !results->currentIndex().isValid()) {
        // ignore enter events
        if (evt->key() == Qt::Key_Enter || evt->key() == Qt::Key_Return)
            return;
    }
    QDialog::keyPressEvent(evt);
}


} // namespace net::tibault::qronos

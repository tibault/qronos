#pragma once

#include "jira/jiraapi.h"
#include "platform/qtpowermonitor.h"

#include <QObject>
#include <QDateTime>

namespace net::tibault::qronos {

class TimeTracker : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "net.tibault.Qronos.TimeTracker")
    Q_PROPERTY(QString currentIssueName
               READ currentIssueName
               NOTIFY currentIssueUpdated)

public:
    TimeTracker(std::shared_ptr<jira::JiraApi> api,
                QObject *parent = nullptr);
    ~TimeTracker();

    bool isTracking() const;
    void startTracking(int64_t id);
    void startTracking(const QString& key);
    void stopTracking();

    jira::Issue currentIssue() const;
    QString currentIssueName() const; // convenience

signals:
    void currentIssueUpdated();
    void idleWarning(bool warn);

protected:
    void setCurrentIssue(const jira::Issue& issue);

    void commitProgress();

private:
    platform::QtPowerMonitor* _pm;

    QTimer _intervalTimer;
    QDateTime _startedAt;

    std::shared_ptr<jira::JiraApi> _api;

    jira::Issue _currentIssue;
    QString _lastWorklogId;

    bool _paused;
};

}

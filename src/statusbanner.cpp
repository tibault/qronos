#include "statusbanner.h"

#include <QGuiApplication>
#include <QMenu>
#include <QPainter>
#include <QPaintEvent>
#include <QPainterPath>
#include <QScreen>
#include <QShowEvent>
#include <QSizePolicy>

namespace net::tibault::qronos {

constexpr int TextMaxLen = 420;
constexpr int TextMarginH = 5;
constexpr int TextMarginV = 2;

StatusBanner::StatusBanner(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint
                    | Qt::WindowStaysOnTopHint
                    | Qt::Tool
                    | Qt::WindowDoesNotAcceptFocus);
    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_MacAlwaysShowToolWindow);
    setSizePolicy(QSizePolicy::Fixed,
                  QSizePolicy::Fixed);

    auto f = font();
    f.setPointSizeF(10.0);
    setFont(f);

    const QColor windowColor(33, 34, 34);
    const QColor textColor(190, 192, 193);

    auto pal = palette();
    pal.setColor(QPalette::Window, windowColor);
    pal.setColor(QPalette::WindowText, textColor);
    setPalette(pal);

    connect(this, &StatusBanner::customContextMenuRequested,
            this, [this](const QPoint& pos){
        if (_menu) {
            _menu->popup(mapToGlobal(pos));
        }
    });

    _warningAnimation = new QVariantAnimation(this);
    _warningAnimation->setStartValue(textColor);
    _warningAnimation->setKeyValueAt(0.5, QColor(255, 55, 55));
    _warningAnimation->setEndValue(textColor);
    _warningAnimation->setDuration(1000);
    _warningAnimation->setEasingCurve(QEasingCurve::InOutSine);
    _warningAnimation->setLoopCount(-1); // infinite

    connect(_warningAnimation, &QVariantAnimation::valueChanged,
            this, [this](){
        update();
    });
}

void StatusBanner::setText(QString txt)
{
    _text = txt.simplified();
    if (_text.isEmpty()) {
        _text = "...";
    }

    setToolTip(text());

    updateGeometry();
    adjustSize();
    positionBanner();
    update();
}

void StatusBanner::showWarning(bool warn)
{
    if (_warning != warn) {
        _warning = warn;
        if (warn) {
            _warningAnimation->start();
        } else {
            _warningAnimation->stop();
            _warningAnimation->setCurrentTime(0); // reset to start value
        }
    }
}

void StatusBanner::clear()
{
    setText({});
    showWarning(false);
}

void StatusBanner::setMenu(QMenu *menu)
{
    _menu = menu;
    setContextMenuPolicy(menu ? Qt::CustomContextMenu
                              : Qt::NoContextMenu);
}

QSize StatusBanner::sizeHint() const
{
    auto t = text(TextMaxLen);
    auto s = fontMetrics().size(0, t);
    return s + QSize(2 * TextMarginH,
                     2 * TextMarginV);
}

QSize StatusBanner::minimumSizeHint() const
{
    return sizeHint();
}

void StatusBanner::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.setOpacity(0.9);

    auto r = event->rect();

    QPainterPath pp;
    pp.addRoundedRect(r, 2.5, 2.5);
    p.fillPath(pp, palette().color(QPalette::Window));

    auto pen = p.pen();
    pen.setColor(_warningAnimation->currentValue().value<QColor>());
    p.setPen(pen);

    p.drawText(r.adjusted(TextMarginH, TextMarginV,
                          -TextMarginH, -TextMarginV),
               text(TextMaxLen));
}

void StatusBanner::showEvent(QShowEvent* event)
{
    positionBanner();

    QWidget::showEvent(event);
}


void StatusBanner::mousePressEvent(QMouseEvent *event)
{
    _movePosition = event->globalPosition();

    // secret feature: snooze warning when clicked with any modifier
    if (_warning && event->modifiers()) {
        showWarning(false);
    }
}

void StatusBanner::mouseMoveEvent(QMouseEvent *event)
{
    const QPoint delta = (event->globalPosition() - _movePosition).toPoint();

    // follow the drag
    move(x()+delta.x(), y()+delta.y());

    _movePosition = event->globalPosition();
}

void StatusBanner::positionBanner()
{
    auto screenRect = QGuiApplication::primaryScreen()->availableGeometry();
    QRect ourRect{pos(), sizeHint()};

    //move ourRect to top center of screenRect
    ourRect.moveCenter(screenRect.center());
    ourRect.moveTop(screenRect.top() + 3);

    move(ourRect.topLeft());
}

QString StatusBanner::text(int elideMax) const
{
    if (elideMax > 0) {
        auto fm = fontMetrics();
        return fm.elidedText(_text,
                             Qt::ElideRight,
                             elideMax);
    }
    return _text;
}

}

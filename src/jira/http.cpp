#include "http.h"

#include <QNetworkReply>

namespace net::tibault::net {

using namespace QtPromise;

namespace {

int
http_status(QNetworkReply* reply)
{
    auto status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    return status.isValid() ? status.toInt() : 0;
}

}

//---------------------------------------------------------------------------//

QByteArray
http_client::authorizationHeader() const
{
    return _auth;
}

void
http_client::setAuthorizationHeader(const QByteArray& auth)
{
    _auth = auth;
}

promise<http_client::Response>
http_client::get(const QUrl &url,
                 const std::map<QString, QString> headers)
{
    auto resolver = [&](const QPromiseResolve<http_client::Response>& resolve,
                        const QPromiseReject<http_client::Response>& reject) {
        QNetworkRequest req{url};
        req.setRawHeader("Accept", "*/*");
        req.setRawHeader("Accept-Language",
                         QLocale::system().uiLanguages()
                                          .join(", ")
                                          .toLatin1());
        if (!_auth.isNull()) {
            req.setRawHeader("Authorization", _auth);
        }

        for (const auto& [key, value] : headers) {
            req.setRawHeader(key.toLatin1(),
                             value.toLatin1());
        }

        auto reply = _nam.get(req);
        QObject::connect(reply, &QNetworkReply::finished, [=]() {
            reply->deleteLater();

            if (int status = http_status(reply)) {
                resolve(http_client::Response {
                    status,
                    reply->readAll()
                });
            } else {
                reject(reply->error());
            }
        });
    };

    return promise<http_client::Response>(resolver);
}

promise<http_client::Response>
http_client::post(const QUrl &url,
                  const QByteArray &body,
                  const std::map<QString, QString> headers)
{
    auto resolver = [&](const QPromiseResolve<http_client::Response>& resolve,
                        const QPromiseReject<http_client::Response>& reject) {
        QNetworkRequest req{url};
        req.setRawHeader("Accept", "*/*");
        req.setRawHeader("Accept-Language",
                         QLocale::system().uiLanguages()
                                          .join(", ")
                                          .toLatin1());
        if (!_auth.isNull()) {
            req.setRawHeader("Authorization", _auth);
        }

        for (const auto& [key, value] : headers) {
            req.setRawHeader(key.toLatin1(),
                             value.toLatin1());
        }

        auto reply = _nam.post(req, body);
        QObject::connect(reply, &QNetworkReply::finished, [=]() {
            reply->deleteLater();

            if (int status = http_status(reply)) {
                resolve(http_client::Response {
                    status,
                    reply->readAll()
                });
            } else {
                reject(reply->error());
            }
        });
    };

    return promise<http_client::Response>(resolver);
}

promise<http_client::Response>
http_client::put(const QUrl &url,
                 const QByteArray &body,
                 const std::map<QString, QString> headers)
{
    auto resolver = [&](const QPromiseResolve<http_client::Response>& resolve,
                        const QPromiseReject<http_client::Response>& reject) {
        QNetworkRequest req{url};
        req.setRawHeader("Accept", "*/*");
        req.setRawHeader("Accept-Language",
                         QLocale::system().uiLanguages()
                                          .join(", ")
                                          .toLatin1());
        if (!_auth.isNull()) {
            req.setRawHeader("Authorization", _auth);
        }

        for (const auto& [key, value] : headers) {
            req.setRawHeader(key.toLatin1(),
                             value.toLatin1());
        }

        auto reply = _nam.put(req, body);
        QObject::connect(reply, &QNetworkReply::finished, [=]() {
            reply->deleteLater();

            if (int status = http_status(reply)) {
                resolve(http_client::Response {
                    status,
                    reply->readAll()
                });
            } else {
                reject(reply->error());
            }
        });
    };

    return promise<http_client::Response>(resolver);
}

//---------------------------------------------------------------------------//

rest_client::rest_client(std::shared_ptr<http_client> http)
    : _http(http ? std::move(http) : std::make_shared<http_client>())
{
    // empty
}

promise<rest_client::Response>
rest_client::get(const QUrl& url)
{
    if (!_http)
        return promise<rest_client::Response>::reject("No http client");

    std::map<QString, QString> headers;
    headers.insert({"Accept", "application/json"});

    return _http->get(url, headers)
    .then([](const http_client::Response& httpResponse){
        try {
            return rest_client::Response {
                httpResponse.status,
                json::parse(httpResponse.body)
            };
        } catch (...) {
            throw httpResponse;
        }
    });
}

promise<rest_client::Response>
rest_client::post(const QUrl &url,
                  const json& body)
{
    if (!_http)
        return promise<rest_client::Response>::reject("No http client");

    std::map<QString, QString> headers;
    headers.insert({"Accept", "application/json"});
    headers.insert({"Content-Type", "application/json"});

    return _http->post(url, body.dump().c_str(), headers)
    .then([](const http_client::Response& httpResponse){
        try {
            return rest_client::Response {
                httpResponse.status,
                json::parse(httpResponse.body)
            };
        } catch (...) {
            throw httpResponse;
        }
    });
}

promise<rest_client::Response>
rest_client::put(const QUrl &url,
                 const json &body)
{
    if (!_http)
        return promise<rest_client::Response>::reject("No http client");

    std::map<QString, QString> headers;
    headers.insert({"Accept", "application/json"});
    headers.insert({"Content-Type", "application/json"});

    return _http->put(url, body.dump().c_str(), headers)
    .then([](const http_client::Response& httpResponse){
        try {
            return rest_client::Response {
                httpResponse.status,
                json::parse(httpResponse.body)
            };
        } catch (...) {
            throw httpResponse;
        }
    });
}

}

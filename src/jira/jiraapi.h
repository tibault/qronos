#pragma once

#include "http.h"

namespace net::tibault::jira {

// https://developer.atlassian.com/cloud/jira/platform/rest/v3

template <typename T>
using promise = QtPromise::QPromise<T>;

class UnauthenticatedException : public std::runtime_error
{
public:
    explicit UnauthenticatedException(const std::string& why);
    explicit UnauthenticatedException();
};

struct AuthInfo //!TODO should probably replace this with OAuth2
{
    QString domain;
    QString email;
    QString token;
};

struct Project
{
    QString id;
    QString key;
    QString name;
};

struct Issue
{
    int64_t id = 0;
    QString key;
    QString summary;
    QString img;
};

struct User
{
    QString id;
    QString email;
    QString name;
};

class JiraApi
{
public:
    JiraApi();

    AuthInfo authInfo() const;
    bool hasAuthInfo() const;
    promise<User> login(AuthInfo auth);

    promise<User> whoAmI();

    promise<QVector<Project>> listProjects();

    promise<Issue> getIssue(int64_t id);
    promise<Issue> getIssue(QString key);

    QUrl webUrl(const Project& project) const;
    QUrl webUrl(const Issue& issue) const;

    promise<QVector<Issue>> findIssues(QString query,
                                       QString projectId = {},
                                       bool assignedToMe = false);

    promise<QVector<Issue>> nextIssues(QString current = {});

    promise<QByteArray> getBlob(const QUrl& url);

    promise<QString> logWork(int64_t id,
                             QDateTime start,
                             QDateTime end = QDateTime::currentDateTime(),
                             QString lastUpdateId = {});
    promise<QString> logWork(QString key,
                             QDateTime start,
                             QDateTime end = QDateTime::currentDateTime(),
                             QString lastUpdateId = {});

    static QDateTime IsoToDateTime(const QString& datetime);
    static QString DateTimeToIso(const QDateTime& datetime);

private:
    std::shared_ptr<net::http_client> _http;
    std::unique_ptr<net::rest_client> _rest;

    AuthInfo _auth;
    QUrl _apiBase;
};

}

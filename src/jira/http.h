#pragma once

#include <QNetworkAccessManager>
#include <QtPromise>
#include <nlohmann/json.hpp>

namespace net::tibault::net {

template <typename T>
using promise = QtPromise::QPromise<T>;

using json = nlohmann::json;

class http_client
{
public:
    struct Response {
        int status;
        QByteArray body;
    };

    QByteArray authorizationHeader() const;
    void setAuthorizationHeader(const QByteArray& auth);

    promise<Response> get(const QUrl& url,
                          const std::map<QString, QString> headers = {});
    promise<Response> post(const QUrl& url,
                           const QByteArray& body,
                           const std::map<QString, QString> headers = {});
    promise<Response> put(const QUrl& url,
                          const QByteArray& body,
                          const std::map<QString, QString> headers = {});
private:
    QNetworkAccessManager _nam;
    QByteArray _auth;
};

class rest_client
{
public:
    struct Response {
        int status;
        json body;
    };

    explicit rest_client(std::shared_ptr<http_client> http = {});

    promise<Response> get(const QUrl& url);
    promise<Response> post(const QUrl& url,
                           const json& body);
    promise<Response> put(const QUrl& url,
                          const json& body);

private:
    std::shared_ptr<http_client> _http;
};

}

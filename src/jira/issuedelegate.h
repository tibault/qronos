#pragma once

#include <QStyledItemDelegate>

namespace net::tibault::jira {

class IssueDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit IssueDelegate(QObject *parent = nullptr);

    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;
};

}

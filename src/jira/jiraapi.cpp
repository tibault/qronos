#include "jiraapi.h"

#include <nlohmann/json.hpp>
#include <QNetworkReply>
#include <QSettings>
#include <QUrlQuery>
#include <QDateTime>

#include <set>

namespace net::tibault::jira {

// get ticket info: issue/BS-1 ?fields=assignee,summary,timetracking
// my filters: filter/my
// all filters: filter/search
// all projects: project/search
// me: /myself
// issues: issue/picker
// locale (works even when not logged in): mypreferences/locale

static const auto API_BASE = QStringLiteral("/rest/api/3/");

static const auto API_MYSELF = QStringLiteral("myself");
static const auto API_PROJECTS = QStringLiteral("project/search");
static const auto API_ISSUE = QStringLiteral("issue/%1");
static const auto API_ISSUES = QStringLiteral("issue/picker");
static const auto API_WORKLOG = QStringLiteral("issue/%1/worklog");

UnauthenticatedException::UnauthenticatedException(const std::string &why)
    : std::runtime_error(why)
{
    // empty
}

UnauthenticatedException::UnauthenticatedException()
    : UnauthenticatedException("Invalid credentials")
{
    // empty
}

JiraApi::JiraApi()
{
    _http = std::make_shared<net::http_client>();
    _rest = std::make_unique<net::rest_client>(_http);

    QSettings s;
    login({
        s.value("jira/domain").toString(),
        s.value("jira/email").toString(),
        s.value("jira/token").toString()
    });
}

AuthInfo JiraApi::authInfo() const
{
    return _auth;
}

bool JiraApi::hasAuthInfo() const
{
    return !_auth.domain.isEmpty()
            && !_auth.email.isEmpty()
            && !_auth.token.isEmpty();
}

promise<User> JiraApi::login(AuthInfo auth)
{
    // we're avoiding wonky QUrl parsing behaviour, by starting with a valid
    // base URL, and copying the desired parts from the user supplied URL
    QUrl apiBase{"https://atlassian.net"};

    if (!auth.domain.isEmpty()) {
        // fixup possible issues with domain
        auto url = QUrl::fromUserInput(auth.domain);
        apiBase.setHost(auth.domain = url.host());
    }

    QSettings s;
    s.setValue("jira/domain", auth.domain);
    s.setValue("jira/email", auth.email);
    s.setValue("jira/token", auth.token);

    _auth = std::move(auth);
    _apiBase = std::move(apiBase);

    if (hasAuthInfo()) {
        auto header = QStringLiteral("%1:%2").arg(_auth.email,
                                                  _auth.token)
                                             .toLatin1()
                                             .toBase64()
                                             .prepend("Basic ");
        _http->setAuthorizationHeader(header);
        return whoAmI();
    } else {
        _http->setAuthorizationHeader({});
        return promise<User>::resolve({});
    }
}

promise<User> JiraApi::whoAmI()
{
    if (!hasAuthInfo())
        return promise<User>::resolve({});

    QUrl url{_apiBase};
    url.setPath(API_BASE + API_MYSELF);

    return _rest->get(url)
    .then([](const net::rest_client::Response& resp){
        if (resp.status == 200) {
            User me{};
            if (resp.body.contains("accountId"))
                me.id = QString::fromStdString(resp.body["accountId"]);
            if (resp.body.contains("emailAddress"))
                me.email = QString::fromStdString(resp.body["emailAddress"]);
            if (resp.body.contains("displayName"))
                me.name = QString::fromStdString(resp.body["displayName"]);
            return me;
        } else if (resp.status == 401) {
            throw UnauthenticatedException();
        } else {
            throw std::runtime_error("Unknown error "
                                        + std::to_string(resp.status));
        }
    });
}

// 200 ok, 400 invalid, 401 auth, 404 not found
promise<QVector<Project> > JiraApi::listProjects()
{
    QUrl url{_apiBase};
    url.setPath(API_BASE + API_PROJECTS);

    return _rest->get(url)
    .then([](const net::rest_client::Response& reply){
        QVector<Project> projects;
        if (reply.status == 200) {
            // isLast bool
            // maxResults int
            // startAt int
            // total int
            // values array
            if (reply.body.contains("values")
                    && reply.body.at("values").is_array()) {
                for (const auto& p : reply.body.at("values")) {
                    try {
                        projects << Project {
                            QString::fromStdString(p["id"]),
                            QString::fromStdString(p["key"]),
                            QString::fromStdString(p["name"])
                        };
                    } catch (...) {}
                }
            }
        }
        return projects;
    });
}

promise<Issue> JiraApi::getIssue(int64_t id)
{
    return getIssue(QString::number(id));
}

//200 ok, 401 auth, 404 not found/no perm
promise<Issue> JiraApi::getIssue(QString key)
{
    QUrl url{_apiBase};
    url.setPath(API_BASE + API_ISSUE.arg(key));

    QUrlQuery q;
    q.addQueryItem("fields",
                   "summary"); // assignee,timetracking
    url.setQuery(q);

    return _rest->get(url).then([](const net::rest_client::Response& reply){
        Issue s{};

        if (reply.status == 200) {
            try {
                s.id = QString::fromStdString(reply.body["id"]).toLongLong();
                s.key = QString::fromStdString(reply.body["key"]);
                s.summary = QString::fromStdString(reply.body["fields"]["summary"]);
            } catch (const std::exception& ex) {
                qDebug() << "ERROR" << ex.what();
            }
        }

        return s;
    });
}

QUrl JiraApi::webUrl(const Project &project) const
{
    QUrl url(_apiBase);
    url.setPath(QStringLiteral("/projects/%1").arg(project.key));
    return url;
}

QUrl JiraApi::webUrl(const Issue &issue) const
{
    QUrl url(_apiBase);
    url.setPath(QStringLiteral("/browse/%1").arg(issue.key));
    return url;
}

//200 ok, 401 auth
promise<QVector<Issue> > JiraApi::findIssues(QString query,
                                             QString projectId,
                                             bool assignedToMe)
{
    QUrl url{_apiBase};
    url.setPath(API_BASE + API_ISSUES);

    QUrlQuery q;
    if (!query.isEmpty()) {
        q.addQueryItem("query", query);
    }
    if (!projectId.isEmpty()) {
        q.addQueryItem("currentProjectId", projectId);
    }
    q.addQueryItem("showSubTasks", "true");
    q.addQueryItem("currentJQL",
                   assignedToMe ? "assignee in (currentUser())" : "");
    url.setQuery(q);

    return _rest->get(url)
    .then([base = _apiBase.toString()](const net::rest_client::Response& resp){
        std::set<int64_t> ids;
        QVector<Issue> out;
        if (resp.status == 200) {
            const auto& sections = resp.body["sections"];
            if (sections.is_array()) {
                for (auto section : sections) {
                    const auto& issues = section["issues"];
                    if (issues.is_array()) {
                        for (const auto& p : issues) {
                            auto [it, newIssue] = ids.insert(p["id"].get<int64_t>());
                            if (newIssue) {
                                out << Issue {
                                    p["id"],
                                    QString::fromStdString(p["key"]),
                                    QString::fromStdString(p["summaryText"]),
                                    base + QString::fromStdString(p["img"])
                                };
                            }
                        }
                    }
                }
            }
        } else {
            // throw something + add try/catch above
        }
        return out;
    });
}

//200 ok, 401 auth
promise<QVector<Issue> > JiraApi::nextIssues(QString current)
{
    QUrl url{_apiBase};
    url.setPath(API_BASE + API_ISSUES);

    QUrlQuery q;
    q.addQueryItem("currentJQL",
                   "status in (\"In Progress\", \"Next\") "
                   "AND assignee in (currentUser())");
    if (!current.isEmpty()) {
        q.addQueryItem("currentIssueKey", current);
    }
    url.setQuery(q);

    return _rest->get(url)
    .then([base = _apiBase.toString()](const net::rest_client::Response& resp){
        QVector<Issue> out;
        if (resp.status == 200) {
            const auto& sections = resp.body["sections"];
            if (sections.is_array() && sections.size() > 0) {
                const auto& issues = sections[0]["issues"];
                if (issues.is_array()) {
                    for (const auto& p : issues) {
                        out << Issue {
                            p["id"],
                            QString::fromStdString(p["key"]),
                            QString::fromStdString(p["summaryText"]),
                            base + QString::fromStdString(p["img"])
                        };
                    }
                }
            }
        } else {
            // throw something + add try/catch above
        }
        return out;
    });
}

promise<QByteArray> JiraApi::getBlob(const QUrl &url)
{
    return _http->get(url).then([](const net::http_client::Response& resp){
        return resp.body;
    });
}

promise<QString> JiraApi::logWork(int64_t id,
                                  QDateTime start,
                                  QDateTime end,
                                  QString lastUpdateId)
{
    if (id <= 0)
        return promise<QString>::reject(QStringLiteral("Invalid issue id"));

    return logWork(QString::number(id), start, end, lastUpdateId);
}

promise<QString> JiraApi::logWork(QString key,
                                  QDateTime start,
                                  QDateTime end,
                                  QString lastUpdateId)
{
    if (start >= end)
        return promise<QString>::resolve({}); // should maybe be a reject?

    auto alreadyLogged = promise<int64_t>::resolve(0);
    if (!lastUpdateId.isEmpty()) {
        // to update the last worklog, first test if it started on the same
        // day and ended within 60s of the next log
        QUrl url{_apiBase};
        url.setPath((API_BASE + API_WORKLOG + "/%2").arg(key, lastUpdateId));
        alreadyLogged = _rest->get(url)
        .then([=](const net::rest_client::Response& reply) -> int64_t {
            if (reply.status == 200) {
                int64_t timeSpent = reply.body["timeSpentSeconds"];
                auto started = IsoToDateTime(
                                    QString::fromStdString(
                                        reply.body["started"]));
                if (started.isValid()
                        && started.date() == start.date()) {
                    auto lastEnd = started.addSecs(timeSpent);
                    auto idleTime = lastEnd.secsTo(start);
                    if (lastEnd <= start && idleTime <= 5 * 60) {
                        return timeSpent + idleTime;
                    }
                }
            }
            return 0;
        })
        .fail([]() -> int64_t {
            return 0;
        });
    }

    return alreadyLogged.then([=](int64_t time) {
        if (time > 0) {
            QUrl url{_apiBase};
            url.setPath((API_BASE + API_WORKLOG + "/%2").arg(key,
                                                             lastUpdateId));
            QUrlQuery q;
            q.addQueryItem("notifyUsers", "false");
            url.setQuery(q);

            net::json body;
            body["timeSpentSeconds"] = time + start.secsTo(end);

            return _rest->put(url, body);
        } else {
            QUrl url{_apiBase};
            url.setPath((API_BASE + API_WORKLOG).arg(key));

            QUrlQuery q;
            q.addQueryItem("notifyUsers", "false");
            url.setQuery(q);

            net::json body;
            body["timeSpentSeconds"] = start.secsTo(end);
            body["started"] = DateTimeToIso(start).toUtf8();

            return _rest->post(url, body);
        }
    })
    .then([](const net::rest_client::Response& reply){
        if (reply.status == 200 || reply.status == 201) {
            return QString::fromStdString(reply.body["id"]);
        }
        return QString{};
    })
    .fail([](){
        return QString{};
    });
}

QDateTime JiraApi::IsoToDateTime(const QString& datetime)
{
    // "2019-12-21T14:59:00.715-0600" ->
    // QDateTime(2019-12-21 14:59:00.715 UTC-06:00 Qt::OffsetFromUTC -21600 s )
    return QDateTime::fromString(datetime,
                                 Qt::ISODateWithMs);
}

QString JiraApi::DateTimeToIso(const QDateTime& datetime)
{
    // I can't get Qt to format the time correctly using TimeSpecs...
    // It keeps either dropping the offset, or forcing the UTC "Z".
    // Even when it does work, it's still not accepted by Jira, so f*ck it,
    // I'll format it myself:

    // only format accepted by Jira: yyyy-MM-ddTHH:mm:ss.zzz±hhmm
    auto output = datetime.toString("yyyy-MM-ddTHH:mm:ss.zzz");

    // now derive the timezone offset from the Qt offset (which is in seconds)
    int utcOffset = datetime.offsetFromUtc();  // eg: -21600 -> -0600
    QChar sign = utcOffset < 0 ? '-' : '+';    // -> -
    utcOffset = abs(utcOffset);
    int hours = (utcOffset / 3600);            // -> 6
    int mins  = (utcOffset % 3600) / 60;       // -> 0

    output += QStringLiteral("%1%2%3").arg(sign)
                                      .arg(hours, 2, 10, QChar('0'))
                                      .arg(mins, 2, 10, QChar('0'));
    return output;
}

}

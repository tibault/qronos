#include "smarttray.h"
#include "timetracker.h"

#include <QApplication>
#include <QSettings>

#ifdef Q_OS_LINUX
#include "dbus/dbus_timetracker.h"
#endif

int main(int argc, char *argv[])
{
    using namespace net::tibault;

    // SETUP

    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    QCoreApplication::setOrganizationName("tibault.net");
    QCoreApplication::setOrganizationDomain("tibault.net");
    QCoreApplication::setApplicationName("Qronos");

    QSettings::setDefaultFormat(QSettings::IniFormat);

    // CREATE DATA MODELS

    auto jiraApi = std::make_shared<jira::JiraApi>();
    qronos::TimeTracker tracker(jiraApi);

    // CREATE UI

    qronos::SmartTray tray(&tracker,
                           jiraApi);
    tray.show();

#ifdef Q_OS_LINUX
    // Register DBus
    auto session = QDBusConnection::sessionBus();
    if (session.isConnected()
            && session.registerService("net.tibault.Qronos")) {
        auto trackerAdaptor = new TimeTrackerAdaptor(&tracker);
        Q_UNUSED(trackerAdaptor) // magic will take care of the rest
        QDBusConnection::sessionBus().registerObject("/", &tracker);
    }
#endif
    return a.exec();
}

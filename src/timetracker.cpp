#include "timetracker.h"

#include <QSettings>
#include <QDebug>

namespace net::tibault::qronos {

using namespace std::chrono_literals;

TimeTracker::TimeTracker(std::shared_ptr<jira::JiraApi> api,
                         QObject *parent)
    : QObject(parent)
    , _api(std::move(api))
    , _paused(false)
{
    _pm = new platform::QtPowerMonitor(this);
    connect(_pm, &platform::QtPowerMonitor::screenLocked,
            this, [=](){
        QSettings s;
        if (s.value("qronos/pauseOnLock", true).toBool()) {
            _intervalTimer.stop();
            commitProgress();
            _paused = true;
        }
    });
    connect(_pm, &platform::QtPowerMonitor::screenUnlocked,
            this, [=](){
        if (_paused || !_currentIssue.id) {
            _startedAt = QDateTime::currentDateTime();
            _paused = false;
            _intervalTimer.start();
        }
    });
    connect(_pm, &platform::QtPowerMonitor::aboutToSuspend,
            this, &TimeTracker::stopTracking);
    connect(_pm, &platform::QtPowerMonitor::resumedFromSleep,
            this, [this](){
        _startedAt = QDateTime::currentDateTime();
        _intervalTimer.start();
    });

    // update worklogs every 15 minutes
    _intervalTimer.setInterval(15min);
    connect(&_intervalTimer, &QTimer::timeout,
            this, &TimeTracker::commitProgress);
    _startedAt = QDateTime::currentDateTime();
    _intervalTimer.start();
}

TimeTracker::~TimeTracker()
{
    commitProgress();
}

bool TimeTracker::isTracking() const
{
    return _currentIssue.id;
}

void TimeTracker::startTracking(int64_t id)
{
    if (_currentIssue.id == id)
        return;
    if (_currentIssue.id)
        stopTracking();

    QPointer self{this};
    _api->getIssue(id).then([self](const jira::Issue& issue){
        if (self) {
            self->setCurrentIssue(issue);
        }
    });
}

void TimeTracker::startTracking(const QString &key)
{
    if (_currentIssue.key == key)
        return;
    if (_currentIssue.id)
        stopTracking();

    QPointer self{this};
    _api->getIssue(key).then([self](const jira::Issue& issue){
        if (self) {
            self->setCurrentIssue(issue);
        }
    });
}

void TimeTracker::stopTracking()
{
    if (_currentIssue.id) {
        commitProgress();

        setCurrentIssue({});
    }
}

jira::Issue TimeTracker::currentIssue() const
{
    return _currentIssue;
}

QString TimeTracker::currentIssueName() const
{
    if (!_currentIssue.id)
        return {};
    return QStringLiteral("%1 %2").arg(_currentIssue.key,
                                       _currentIssue.summary);
}

void TimeTracker::setCurrentIssue(const jira::Issue &issue)
{
    if (_currentIssue.id == issue.id)
        return;

    if (!issue.id || issue.key.isEmpty()) {
        _currentIssue = {};
    } else {
        _currentIssue = issue;
    }

    _intervalTimer.start(); // restart timer
    _startedAt = QDateTime::currentDateTime();
    _lastWorklogId.clear();

    emit idleWarning(false);
    emit currentIssueUpdated();
}

void TimeTracker::commitProgress()
{
    if (_paused)
        return;

    auto endTime = QDateTime::currentDateTime();
    if (_currentIssue.id) {
        if (_startedAt.secsTo(endTime) >= 60) {
            QPointer self{this};
            _api->logWork(QString::number(_currentIssue.id),
                          _startedAt,
                          endTime,
                          _lastWorklogId)
            .then([self, key = _currentIssue.key, startTime = _startedAt, endTime](QString logId){
                if (logId.isEmpty()) {
                    qWarning() << "Failed to log time for" << key
                               << "[" << startTime.toString("H:mm")
                               << "->" << endTime.toString("H:mm") << "]";
                } else if (self) {
                    if (self->_currentIssue.key == key) {
                        self->_startedAt = endTime;
                        self->_lastWorklogId = logId;
                    }
                }
            });
        }
    } else {
        // if idle for 15 minutes, emit warning
        if (_startedAt.secsTo(endTime) >= 15*60) {
            emit idleWarning(true);
        }
    }
}

}

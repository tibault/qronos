#include "smarttray.h"
#include "findissuedialog.h"
#include "preferencesdialog.h"

#include <QApplication>
#include <QDesktopServices>
#include <QMenu>
#include <QSettings>

namespace net::tibault::qronos {

static const auto SHOW_BANNER = QStringLiteral("qronos/banner");

bool system_uses_dark_theme()
{
#if defined(Q_OS_WIN)
    return QSettings("\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft"
                     "\\Windows\\CurrentVersion\\Themes\\Personalize",
                     QSettings::NativeFormat).value("SystemUsesLightTheme")
                                             .toUInt() == 0;
#elif defined(Q_OS_MACOS)
    return false; // doesn't matter, macOS uses an icon mask
#else
    return true; // no way to know? Assume dark...
#endif
}

QIcon create_icon(bool dark)
{
    QIcon icon;
    icon.addFile(dark ? ":/svg/tray_dark"
                      : ":/svg/tray_light");
    icon.addFile(dark ? ":/svg/tray_light"
                      : ":/svg/tray_dark",
                 QSize{},
                 QIcon::Selected);
    icon.setIsMask(true);
    return icon;
}

SmartTray::SmartTray(TimeTracker *tracker,
                     std::shared_ptr<jira::JiraApi> api)
    : QSystemTrayIcon(tracker),
      _banner(nullptr),
      _tracker(tracker),
      _api(std::move(api))
{
    assert(_tracker);

    QSettings s;

    _currentIssue = _trayMenu.addAction("⌛︎ Loading...");
    _currentIssue->setEnabled(false);
    {
        auto fontBold = _currentIssue->font();
        fontBold.setBold(true);
        _currentIssue->setFont(fontBold);
    }

    _stopTracking = _trayMenu.addAction("Stop tracking");
    _stopTracking->setVisible(false);
    connect(_stopTracking, &QAction::triggered,
            _tracker, &TimeTracker::stopTracking);

    _showIssue = _trayMenu.addAction("Show issue details...");
    _showIssue->setVisible(false);
    connect(_showIssue, &QAction::triggered,
            this, [=](){
        auto url = _showIssue->data().toUrl();
        if (url.isValid()) {
            QDesktopServices::openUrl(url);
        }
    });

    _trayMenu.addSeparator();

    _findIssue = _trayMenu.addAction("Find issue...");
    connect(_findIssue, &QAction::triggered,
            this, [=](){
        auto findDlg = new FindIssueDialog(_api);
        connect(findDlg, &FindIssueDialog::finished,
                this, [=](int result){
            if (result == QDialog::Accepted) {
                startTracking(findDlg->selectedIssue());
            }
            findDlg->deleteLater();
        });
        findDlg->show();
        findDlg->raise();
    });
    _suggestedIssues = _trayMenu.addMenu("Up next:");

    _trayMenu.addSeparator();

    bool hasBanner = s.value(SHOW_BANNER, true).toBool();
    _showBanner = _trayMenu.addAction("Show status banner");
    _showBanner->setCheckable(true);
    _showBanner->setChecked(hasBanner);
    connect(_showBanner, &QAction::changed,
            this, [=](){
        showBanner(_showBanner->isChecked());
    });

    _preferences = _trayMenu.addAction("Preferences...");
    connect(_preferences, &QAction::triggered,
            this, &SmartTray::openPreferences);

    _trayMenu.addSeparator();

    auto quit = _trayMenu.addAction("Quit");
    connect(quit, &QAction::triggered,
            qApp, &QApplication::quit);

    setIcon(create_icon(!system_uses_dark_theme()));
    setContextMenu(&_trayMenu);

    connect(this, &SmartTray::activated,
            this, &SmartTray::updateMenu);

    connect(tracker, &TimeTracker::currentIssueUpdated,
            this, &SmartTray::updateMenu);
    connect(tracker, &TimeTracker::idleWarning,
            this, [=](bool warn) {
        if (_banner) {
            _banner->showWarning(warn);
        }
    });

    showBanner(hasBanner);
    updateMenu();

    if (!_api->hasAuthInfo()) {
        openPreferences();
    }
}

SmartTray::~SmartTray()
{
    delete _banner;
}

void SmartTray::updateMenu()
{
    QString currentKey;
    if (_tracker->isTracking()) {
        auto issue = _tracker->currentIssue();
        currentKey = issue.key;
        _currentIssue->setText(QStringLiteral("%1 %2").arg(issue.key,
                                                           issue.summary));
        _stopTracking->setVisible(true);
        _showIssue->setData(_api->webUrl(issue));
        _showIssue->setVisible(true);
    } else {
        _currentIssue->setText("Not working...");
        _stopTracking->setVisible(false);
        _showIssue->setData({});
        _showIssue->setVisible(false);
    }
    if (_banner) {
        _banner->setText(_currentIssue->text());
    }

    _api->nextIssues(currentKey)
    .then([=](const QVector<jira::Issue>& issues) {
        _suggestedIssues->clear();

        for (const auto& issue : issues) {
            auto title = QStringLiteral("%1 %2").arg(issue.key,
                                                     issue.summary);
            auto item = _suggestedIssues->addAction(title);
            connect(item, &QAction::triggered,
                    this, std::bind(&SmartTray::startTracking, this, issue.id));
        }
    });
}

void SmartTray::showBanner(bool show)
{
    if (!!_banner == show)
        return;

    QSettings s;
    s.setValue(SHOW_BANNER, show);
    if (!_banner) {
        _banner = new qronos::StatusBanner{};
        _banner->setText(_currentIssue->text());
        _banner->setMenu(&_trayMenu);
        _banner->show();
    } else {
        _banner->deleteLater();
        _banner = nullptr;
    }
}

void SmartTray::openPreferences()
{
    auto diag = new PreferencesDialog(_api);
    connect(diag, &PreferencesDialog::finished,
            diag, &PreferencesDialog::deleteLater);
    diag->show();
    diag->raise();
}

void SmartTray::startTracking(int64_t id)
{
    _tracker->startTracking(id);
}

}

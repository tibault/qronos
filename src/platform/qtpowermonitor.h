#pragma once

#include <QObject>

namespace net::tibault::platform {

class QtPowerMonitor : public QObject
{
    Q_OBJECT

public:
    explicit QtPowerMonitor(QObject* parent = nullptr);
    ~QtPowerMonitor() override;

    QtPowerMonitor(const QtPowerMonitor&) = delete;
    QtPowerMonitor& operator=(const QtPowerMonitor&) = delete;

    QtPowerMonitor(QtPowerMonitor&&) = delete;
    QtPowerMonitor& operator=(QtPowerMonitor&&) = delete;

signals:
    void aboutToSuspend();
    void resumedFromSleep();

    void screenLocked();
    void screenUnlocked();

private:
    void* _impl;
};

}

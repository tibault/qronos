#include "../qtpowermonitor.h"

#include <QAbstractNativeEventFilter>
#include <QApplication>
#include <QDebug>

#ifdef Q_OS_WIN
#include <Windows.h>
#include <WtsApi32.h>
#endif

namespace net::tibault::platform {

/**
 * @brief The NativeWinMessageReceiver class gives you a HWND you can use
 * to register to native Windows events. You can then observe the events with
 * a standard Qt NativeEventFilter.
 */
class NativeWinMessageReceiver
{
public:
    NativeWinMessageReceiver();
    ~NativeWinMessageReceiver();

    NativeWinMessageReceiver(const NativeWinMessageReceiver& other) = delete;
    NativeWinMessageReceiver(NativeWinMessageReceiver&& other) = delete;

    HWND hwnd;
};

class PowerEventFilter : public QAbstractNativeEventFilter
{
public:
    explicit PowerEventFilter(QtPowerMonitor* monitor);
    ~PowerEventFilter() override;
    virtual bool nativeEventFilter(const QByteArray &eventType,
                                   void *message,
                                   qintptr *result) override;
private:
    QtPowerMonitor* _monitor;
    NativeWinMessageReceiver handler;
};

extern "C" LRESULT QT_WIN_CALLBACK tdDummyWndProc(HWND hwnd,
                                                  UINT message,
                                                  WPARAM wParam,
                                                  LPARAM lParam)
{
    // Just pass it on to the default handler
    return DefWindowProc(hwnd, message, wParam, lParam);
}

NativeWinMessageReceiver::NativeWinMessageReceiver()
    : hwnd(nullptr)
{
    WNDCLASSEX nwmr = {};
    nwmr.cbSize = sizeof(WNDCLASSEX);
    nwmr.lpfnWndProc = tdDummyWndProc;
    nwmr.hInstance = static_cast<HINSTANCE>(GetModuleHandle(nullptr));
    nwmr.lpszClassName = L"TDNativeWinMessageReceiver";

    if (RegisterClassEx(&nwmr)) {
        hwnd = CreateWindowEx(0,
                              nwmr.lpszClassName,
                              L"DummyNativeWinMessageReceiver",
                              0,
                              0,
                              0,
                              0,
                              0,
                              HWND_MESSAGE,
                              nullptr,
                              nullptr,
                              nullptr);
    }
}

NativeWinMessageReceiver::~NativeWinMessageReceiver()
{
    if (hwnd) {
        DestroyWindow(hwnd);
        hwnd = nullptr;
    }
}

PowerEventFilter::PowerEventFilter(QtPowerMonitor *monitor)
    : _monitor(monitor)
{
    if (handler.hwnd) {
        WTSRegisterSessionNotification(handler.hwnd,
                                       NOTIFY_FOR_THIS_SESSION);
    }
}

PowerEventFilter::~PowerEventFilter()
{
    if (handler.hwnd) {
        WTSUnRegisterSessionNotification(handler.hwnd);
    }
}

bool PowerEventFilter::nativeEventFilter(const QByteArray &eventType,
                                         void *message,
                                         qintptr *)
{
    // eventType should be windows_generic_MSG or windows_dispatcher_MSG

    auto msg = static_cast<MSG*>(message);

    //qDebug() << eventType << msg->message;

    if (msg->message == WM_POWERBROADCAST) {
        // https://docs.microsoft.com/en-us/windows/win32/power/wm-powerbroadcast
        qDebug()<< eventType << "POWER EVENT";
        switch (msg->wParam) {
        case PBT_APMPOWERSTATUSCHANGE:
            qDebug()<< eventType << "PBT_APMPOWERSTATUSCHANGE received";
            break;
        case PBT_APMRESUMEAUTOMATIC:
            qDebug()<< eventType << "PBT_APMRESUMEAUTOMATIC received";
            break;
        case PBT_APMRESUMESUSPEND:
            qDebug()<< eventType << "PBT_APMRESUMESUSPEND received";
            emit _monitor->resumedFromSleep();
            break;
        case PBT_APMSUSPEND:
            qDebug()<< eventType << "PBT_APMSUSPEND received";
            emit _monitor->aboutToSuspend();
            break;
        }
    } else if (msg->message == WM_WTSSESSION_CHANGE) {
        // https://docs.microsoft.com/en-us/windows/win32/termserv/wm-wtssession-change
        qDebug() << eventType << "SESSION EVENT";
        if (msg->wParam == WTS_SESSION_LOCK) {
            qDebug()<< eventType << "WTS_SESSION_LOCK received";
            emit _monitor->screenLocked();
        } else if (msg->wParam == WTS_SESSION_UNLOCK) {
            qDebug()<< eventType << "WTS_SESSION_UNLOCK received";
            emit _monitor->screenUnlocked();
        }
    }
    return false;
}

QtPowerMonitor::QtPowerMonitor(QObject* parent)
    : QObject(parent)
{
    auto monitor = new PowerEventFilter(this);
    _impl = monitor;
    qApp->installNativeEventFilter(monitor);
}

QtPowerMonitor::~QtPowerMonitor()
{
    delete static_cast<PowerEventFilter*>(_impl);
    _impl = nullptr;
}

}

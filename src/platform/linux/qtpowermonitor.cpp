#include "../qtpowermonitor.h"
#include "qtdbuspowermonitor_p.h"

#include <QDebug>
#include <QtDBus>

namespace net::tibault::platform {

QtPowerMonitor::QtPowerMonitor(QObject* parent)
    : QObject(parent)
{
    auto dbus = new QtDBusPowerMonitor(this);
    _impl = dbus;

    connect(dbus, &QtDBusPowerMonitor::locked,
            this, [this](bool active) {
        if (active) {
            emit screenLocked();
        } else {
            emit screenUnlocked();
        }
    });
    connect(dbus, &QtDBusPowerMonitor::suspended,
            this, [this](bool active) {
        if (active) {
            emit aboutToSuspend();
        } else {
            emit resumedFromSleep();
        }
    });
}

QtPowerMonitor::~QtPowerMonitor()
{
    // empty
}

QtDBusPowerMonitor::QtDBusPowerMonitor(QObject* parent)
    : QObject(parent)
{
    auto system = QDBusConnection::systemBus();
    auto session = QDBusConnection::sessionBus();

    if (system.isConnected()) {
        // systemd (logind) suspend events
        system.connect("org.freedesktop.login1",
                       "/org/freedesktop/login1",
                       "org.freedesktop.login1.Manager",
                       "PrepareForSleep", this, SLOT(prepareSleep(bool)));
        // also handle PrepareForShutdown ?

        // systemd (logind) session events
        if (auto path = sessionPath(); !path.isEmpty()) {
            system.connect("org.freedesktop.login1",
                           path,
                           "org.freedesktop.DBus.Properties",
                           "PropertiesChanged",
                           this, SLOT(propertyChanged(QString,QVariantMap,QStringList)));
        }
    } else {
        qWarning() << "Could not connect to System DBus!";
    }

    if (session.isConnected()) {
        // Gnome session lock events
        session.connect("org.gnome.ScreenSaver",
                        "/org/gnome/ScreenSaver",
                        "org.gnome.ScreenSaver",
                        "ActiveChanged", this, SLOT(setLockedState(bool)));

        // Mate session lock events
        session.connect("org.mate.ScreenSaver",
                        "/org/mate/ScreenSaver",
                        "org.mate.ScreenSaver",
                        "ActiveChanged", this, SLOT(setLockedState(bool)));

        // XFCE session lock events
        session.connect("org.xfce.ScreenSaver",
                        "/org/xfce/ScreenSaver",
                        "org.xfce.ScreenSaver",
                        "ActiveChanged", this, SLOT(setLockedState(bool)));

        // freedesktop.org session lock events (KDE?)
        session.connect("org.freedesktop.ScreenSaver",
                        "/org/freedesktop/ScreenSaver",
                        "org.freedesktop.ScreenSaver",
                        "ActiveChanged", this, SLOT(setLockedState(bool)));
    } else {
        qWarning() << "Could not connect to Session DBus!";
    }
}

QtDBusPowerMonitor::~QtDBusPowerMonitor()
{
    // empty
}

QString QtDBusPowerMonitor::sessionPath() const
{
    auto message = QDBusMessage::createMethodCall(
        QStringLiteral("org.freedesktop.login1"),
        QStringLiteral("/org/freedesktop/login1/user/self"),
        QStringLiteral("org.freedesktop.DBus.Properties"),
        QStringLiteral("Get"));
    message.setArguments({
        QStringLiteral("org.freedesktop.login1.User"),
        QStringLiteral("Display")
    });

    const QDBusMessage reply = QDBusConnection::systemBus().call(message);
    if (reply.type() == QDBusMessage::ReplyMessage) {
        /* example reply contents:
         *
         * QDBusMessage(type=MethodReturn,
         *              service=":1.36",
         *              signature="v",
         *              contents=([Variant:
         *          [Argument: (so) "2",
         *                     [ObjectPath: /org/freedesktop/login1/session/_32]]]) )
         */
        const auto contents = reply.arguments();
        const auto variant = contents.first().value<QDBusVariant>().variant();
        const auto arg = variant.value<QDBusArgument>();

        QString id;
        QDBusObjectPath path;

        arg.beginStructure();
        arg >> id >> path;
        arg.endStructure();

        return path.path();
    }
    return QString();
}

void QtDBusPowerMonitor::prepareSleep(bool par)
{
    emit suspended(par);
}

void QtDBusPowerMonitor::setLockedState(bool active)
{
    emit locked(active);
}

void QtDBusPowerMonitor::propertyChanged(QString name, QVariantMap map, QStringList)
{
    if (name == "org.freedesktop.login1.Session") {
        if (auto it = map.find("LockedHint"); it != map.end()) {
            if (it.value().canConvert<bool>()) {
                setLockedState(it.value().toBool());
            }
        }
    }
}

}

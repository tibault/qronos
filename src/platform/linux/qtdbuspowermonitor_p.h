#pragma once

#include <QObject>

namespace net::tibault::platform {

class QtDBusPowerMonitor : public QObject
{
    Q_OBJECT

public:
    explicit QtDBusPowerMonitor(QObject* parent = nullptr);
    ~QtDBusPowerMonitor() override;

    QtDBusPowerMonitor(const QtDBusPowerMonitor&) = delete;
    QtDBusPowerMonitor& operator=(const QtDBusPowerMonitor&) = delete;

    QtDBusPowerMonitor(QtDBusPowerMonitor&&) = delete;
    QtDBusPowerMonitor& operator=(QtDBusPowerMonitor&&) = delete;

    QString sessionPath() const;

signals:
    void locked(bool);
    void suspended(bool);

private slots:
    void prepareSleep(bool par);
    void setLockedState(bool active);
    void propertyChanged(QString name, QVariantMap map, QStringList list);
};

}

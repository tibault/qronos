#include "../qtpowermonitor.h"

#import <AppKit/AppKit.h>

@interface TDMacPowerMonitor : NSObject {
    net::tibault::platform::QtPowerMonitor* _monitor;
}
- (id) initWithMonitor: (net::tibault::platform::QtPowerMonitor*) monitor;
- (void) dealloc;
- (void) receiveSleepNote: (NSNotification*) note;
- (void) receiveWakeNote: (NSNotification*) note;
- (void) screenLocked: (NSNotification*) note;
- (void) screenUnlocked: (NSNotification*) note;
@end

@implementation TDMacPowerMonitor

- (id) initWithMonitor: (net::tibault::platform::QtPowerMonitor*) monitor
{
    if ((self = [super init])) {
        _monitor = monitor;

        // https://developer.apple.com/library/archive/qa/qa1340/_index.html
        auto nc = [[NSWorkspace sharedWorkspace] notificationCenter];
        [nc addObserver: self
               selector: @selector(receiveSleepNote:)
                   name: NSWorkspaceWillSleepNotification
                 object: nil];
        [nc addObserver: self
               selector: @selector(receiveWakeNote:)
                   name: NSWorkspaceDidWakeNotification
                 object: nil];

        // https://kanecheshire.com/blog/2014/10/13/351/
        auto dc = [NSDistributedNotificationCenter defaultCenter];
        [dc addObserver:self
               selector:@selector(screenLocked:)
                   name:@"com.apple.screenIsLocked"
                 object:nil];
        [dc addObserver:self
               selector:@selector(screenUnlocked:)
                   name:@"com.apple.screenIsUnlocked"
                 object:nil];
    }
    return self;
}

- (void) dealloc {
    auto nc = [[NSWorkspace sharedWorkspace] notificationCenter];
    [nc removeObserver: self];

    auto dc = [NSDistributedNotificationCenter defaultCenter];
    [dc removeObserver: self];

    [super dealloc];
}

- (void) receiveSleepNote: (NSNotification*) note
{
    Q_UNUSED(note)
    if (_monitor) emit _monitor->aboutToSuspend();
}

- (void) receiveWakeNote: (NSNotification*) note
{
    Q_UNUSED(note)
    if (_monitor) emit _monitor->resumedFromSleep();
}

- (void) screenLocked: (NSNotification*) note
{
    Q_UNUSED(note)
    if (_monitor) emit _monitor->screenLocked();
}

- (void) screenUnlocked: (NSNotification*) note
{
    Q_UNUSED(note)
    if (_monitor) emit _monitor->screenUnlocked();
}

@end

namespace net::tibault::platform {

QtPowerMonitor::QtPowerMonitor(QObject* parent)
    : QObject(parent)
{
    auto monitor = [[TDMacPowerMonitor alloc] initWithMonitor:this];
    _impl = monitor;
}

QtPowerMonitor::~QtPowerMonitor()
{
    if (_impl) {
        auto monitor = static_cast<TDMacPowerMonitor*>(_impl);
        [monitor release];
        _impl = nullptr;
    }
}

}

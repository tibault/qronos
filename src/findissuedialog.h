#pragma once

#include <QComboBox>
#include <QDialog>
#include <QListView>

#include "jira/jiraapi.h"

namespace net::tibault::qronos {

class IssueResultModel : public QAbstractListModel
{
public:
    enum Roles {
        IdRole = Qt::UserRole,
        UrlRole,
    };
    explicit IssueResultModel(const std::shared_ptr<jira::JiraApi>& api,
                              QObject* parent = nullptr);

    void suggestIssues(const QString& query,
                       const QString &project,
                       bool assignedToMe);

    virtual int rowCount(const QModelIndex &parent = {}) const override;
    virtual QVariant data(const QModelIndex &index,
                          int role) const override;
private:
    QIcon fetchIcon(const QString& url) const;

    std::shared_ptr<jira::JiraApi> _api;
    QVector<jira::Issue> _issues;

    mutable QMap<QString, QIcon> _iconCache;
};

class FindIssueDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FindIssueDialog(const std::shared_ptr<jira::JiraApi>& api,
                             QWidget *parent = nullptr);
    int64_t selectedIssue() const;

    void keyPressEvent(QKeyEvent *) override;

private:
    std::shared_ptr<jira::JiraApi> _api;
    int64_t _selectedIssue;

    QListView* results;
};

}




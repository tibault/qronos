#pragma once

#include <QWidget>
#include <QVariantAnimation>

class QMenu;

namespace net::tibault::qronos {

class StatusBanner : public QWidget
{
    Q_OBJECT
public:
    explicit StatusBanner(QWidget *parent = nullptr);

    void setText(QString text);
    void showWarning(bool warn);
    void clear();

    void setMenu(QMenu* menu);

    virtual QSize sizeHint() const override;
    virtual QSize minimumSizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void showEvent(QShowEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;

private:
    void positionBanner();
    QString text(int elideMax = 0) const;

    QString _text;
    QMenu* _menu;
    QPointF _movePosition;

    QVariantAnimation* _warningAnimation;
    bool _warning = false;
};

}

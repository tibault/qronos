# Qronos - Jira time tracking

## What is it?

Qronos is a glorified stopwatch, implemented as a small system tray application that integrates with your Atlassian Jira cloud.

Choose what issue you're working on with the built-in UI, and Qronos will start tracking time until you press stop.
Every 15 minutes it will either create a new worklog for the active issue, or (if there's no gap) update the previous one.

Time tracking will automatically pause when the screensaver activates, and will stop when your computer goes to sleep.

## Supported platforms

* Linux (tested on: Ubuntu 19.10, Ubuntu 18.04, Arch)
* macOS (tested on: macOS 10.15)
* Windows (purely theoretical powermanager implementation, untested)

## How to build
### Requirements

You'll need CMake, Qt6 (tested on 6.2.2), and a compiler that supports C++17.

```
git clone git@gitlab.com:tibault/qronos.git
cd qronos
git submodule update --init
mkdir build
cd build
cmake ..
make/ninja/nmake
```

## Current state

It's usable, everything I mentioned above works. I haven't worked on packaging yet though, so you'll have to build it and run it as is.

I'm still working on better integration in Gnome and XFCE (I want to see the active issue in the top panel), smarter tracking (auto pause on lunch break?), issue transitions (go to "In Progress" when tracking, or assign for review when done), etc
